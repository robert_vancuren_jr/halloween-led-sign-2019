#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

  #define BRIGHTNESS_PIN A1
  #define MODE_PIN 0

  #define NEO_PIXEL_PIN 1 
  #define NUMPIXELS 5

  Adafruit_NeoPixel pixels(NUMPIXELS, NEO_PIXEL_PIN, NEO_GRB + NEO_KHZ800);

  #define DELAYVAL 500

  int currentPixel = 0;
  unsigned long lastUpdate;
  unsigned long lastMode;

  uint32_t colors[3] = {pixels.Color(255, 0, 0), pixels.Color(0, 255, 0), pixels.Color(0, 0, 255)};

  int currentMode = 0;
  int modesLength = 3;

void setup() {
  pinMode(BRIGHTNESS_PIN, INPUT);
  pinMode(MODE_PIN, INPUT_PULLUP);
  
  pixels.begin();

  lastUpdate = millis();
  lastMode = millis();
}

void loop() {
  unsigned long now = millis();
  unsigned long pixelDelta = now - lastUpdate;
  unsigned long modeDelta = now - lastMode;

  if (modeDelta > 200) {
    bool modePressed = digitalRead(MODE_PIN) == LOW;
    
    if (modePressed) {
      currentMode = (currentMode + 1) % modesLength;
      lastMode = now;
    }
  }

  if (pixelDelta > 100) {
    currentPixel = (currentPixel + 1) % NUMPIXELS;
    lastUpdate = now;
  }


  int brightnessInput = analogRead(BRIGHTNESS_PIN);
  int brightness = map(brightnessInput, 0, 1023, 0, 255);

  

  pixels.clear();

  pixels.setBrightness(brightness);
  pixels.setPixelColor(currentPixel, colors[currentMode]);

  pixels.show();

}
